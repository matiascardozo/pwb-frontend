import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import { Component, default as React } from 'react';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import './App.css';
import './index.css';
import ExposedFunctionTable from './Table';
import 'react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';


class App extends Component {
  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-sm">
          <div class="input-group input-group-sm mb-3">

            <ExposedFunctionTable />
            </div>

          </div>
        </div>

      </div>
    );
  }
}

export default App;

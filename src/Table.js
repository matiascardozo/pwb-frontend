import BootstrapTable from "react-bootstrap-table-next";
import React, { Component } from "react";
import filterFactory, {
  selectFilter,
  dateFilter,
  Comparator,
  customFilter
} from "react-bootstrap-table2-filter";
import paginationFactory from "react-bootstrap-table2-paginator";
import PropTypes from "prop-types";

const products = [
  {
    id: 1,
    local: "Locaal  1",
    sucursal: "Suc",
    fecha: new Date().toString(),
    horaFin: new Date().toLocaleTimeString(),
    horaInicio: new Date().toLocaleTimeString(),
    cliente: "cliente1 ",
    especialidad: "esp 1",
    servicio: "servicio 1",
    empleado: "emple 1",
    estado: "estado 1",
    asistio: "si"
  }
];

const filterDate = filterVal =>
  products.filter(producto => {
    const { desde, hasta } = filterVal.fecha;
    if (desde && hasta)
      return (
        new Date(producto.fecha) >= new Date(filterVal.fecha.desde) &&
        new Date(producto.fecha) <= new Date(filterVal.fecha.hasta)
      );
    else if (desde) {
      return new Date(producto.fecha) >= new Date(filterVal.fecha.desde);
    } else if (hasta) {
      return new Date(producto.fecha) <= new Date(filterVal.fecha.hasta);
    }
  });

const selectOptions = {
  0: "good",
  1: "Bad",
  2: "unknown"
};

const columns = [
  {
    dataField: "id",
    text: "id",
    hidden: true
  },
  {
    dataField: "local",
    text: "Local",
    sort: true,
    filter: selectFilter({
      options: selectOptions,
      className: "form-control-sm"
    })
  },
  {
    dataField: "sucursal",
    text: "Sucursal",
    sort: true,
    filter: selectFilter({
      options: selectOptions,
      className: "form-control-sm"
    })
  },
  {
    dataField: "fecha",
    text: "Fecha",
    sort: true,
    filter: customFilter({
      onFilter: filterDate
    }),
    filterRenderer: (onFilter, column) => (
      <DateFilter onFilter={onFilter} column={column} />
    )
  },

  {
    dataField: "horaInicio",
    text: "Hora inicio",
    sort: true
  },
  {
    dataField: "horaFin",
    text: "Hora fin",
    sort: true
  },
  {
    dataField: "cliente",
    text: "Cliente",
    sort: true,
    filter: selectFilter({
      options: selectOptions,
      className: "form-control-sm"
    })
  },
  {
    dataField: "especialidad",
    text: "Especialidad",
    sort: true,
    filter: selectFilter({
      options: selectOptions,
      className: "form-control-sm"
    })
  },
  {
    dataField: "servicio",
    text: "Servicio",
    filter: selectFilter({
      options: selectOptions,
      className: "form-control-sm"
    })
  },
  {
    dataField: "empleado",
    text: "Empleado",
    sort: true,
    filter: selectFilter({
      options: selectOptions,
      className: "form-control-sm"
    })
  },
  {
    dataField: "estado",
    text: "Estado",
    sort: true,
    filter: selectFilter({
      options: selectOptions,
      className: "form-control-sm"
    })
  },
  {
    dataField: "asistio",
    text: "Asistió",
    sort: true,
    filter: selectFilter({
      options: selectOptions,
      className: "form-control-sm"
    })
  },
  {
    dataField: "acciones",
    isDummyField: true,
    text: "Acciones",
    formatter: (cellContent, row) => {
      return (
        <React.Fragment>
          <a href="#" class="badge badge-pill badge-info">
            Observacion
          </a>
          <a href="#" class="badge badge-pill badge-success">
            Asistio
          </a>
          <a href="#" class="badge badge-pill badge-danger">
            No asistio
          </a>
          <a href="#" class="badge badge-pill badge-warning">
            Cancelar
          </a>
        </React.Fragment>
      );
    }
  }
];

const defaultSorted = [
  {
    dataField: "id",
    order: "desc"
  }
];

export default class ExposedFunctionTable extends React.Component {

    handleTableChange = (type, { page, sizePerPage, filters, sortField, sortOrder, cellEdit }) => {
        const currentIndex = (page - 1) * sizePerPage;
        setTimeout(() => {
          console.log(type,  page, sizePerPage, filters, sortField, sortOrder, cellEdit )
          let result = products;
    
          // Handle column filters
          result = result.filter((row) => {
            let valid = true;
            for (const dataField in filters) {
              const { filterVal, filterType, comparator } = filters[dataField];
    
              if (filterType === 'TEXT') {
                if (comparator === Comparator.LIKE) {
                  valid = row[dataField].toString().indexOf(filterVal) > -1;
                } else {
                  valid = row[dataField] === filterVal;
                }
              }
              if (!valid) break;
            }
            return valid;
          });
          // Handle column sort
          if (sortOrder === 'asc') {
            result = result.sort((a, b) => {
              if (a[sortField] > b[sortField]) {
                return 1;
              } else if (b[sortField] > a[sortField]) {
                return -1;
              }
              return 0;
            });
          } else {
            result = result.sort((a, b) => {
              if (a[sortField] > b[sortField]) {
                return -1;
              } else if (b[sortField] > a[sortField]) {
                return 1;
              }
              return 0;
            });
          }
          this.setState(() => ({
            page,
            data: result.slice(currentIndex, currentIndex + sizePerPage),
            totalSize: result.length,
            sizePerPage
          }));
        }, 2000);
      }

  handleGetCurrentPage = () => {
    console.log(this.node.paginationContext.currPage);
  };

  handleGetCurrentSizePerPage = () => {
    console.log(this.node.paginationContext.currSizePerPage);
  };

  handleGetCurrentSortColumn = () => {
    console.log(this.node.sortContext.state.sortColumn);
  };

  handleGetCurrentSortOrder = () => {
    console.log(this.node.sortContext.state.sortOrder);
  };

  handleGetCurrentFilter = () => {
    console.log(this.node.filterContext.currFilters);
  };

  render() {
    return (
      <div>
        <button className="btn btn-default" onClick={this.handleGetCurrentPage}>
          Get Current Page
        </button>
        <button
          className="btn btn-default"
          onClick={this.handleGetCurrentSizePerPage}
        >
          Get Current Size Per Page
        </button>
        <button
          className="btn btn-default"
          onClick={this.handleGetCurrentSortColumn}
        >
          Get Current Sort Column
        </button>
        <button
          className="btn btn-default"
          onClick={this.handleGetCurrentSortOrder}
        >
          Get Current Sort Order
        </button>
        <button
          className="btn btn-default"
          onClick={this.handleGetCurrentFilter}
        >
          Get Current Filter Information
        </button>

        <BootstrapTable
          keyField="id"
          data={products}
          columns={columns}
          bootstrap4
          striped
          hover
          condensed
          remote
          onTableChange={ this.handleTableChange }

          noDataIndication="Tabla vacía"
          caption="PWB 1er Parcial"
          ref={n => (this.node = n)}
          filter={filterFactory()}
          pagination={paginationFactory()}
          defaultSorted={defaultSorted}
          className="table-responsive"
          styleName="table-responsive"
        />
      </div>
    );
  }
}

class DateFilter extends React.Component {
  static propTypes = {
    column: PropTypes.object.isRequired,
    onFilter: PropTypes.func.isRequired
  };
  constructor(props) {
    super(props);
    this.filter = this.filter.bind(this);
    this.getValue = this.getValue.bind(this);
    this.onChange = this.onChange.bind(this);
    this.state = { desde: "", hasta: "" };
  }
  onChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });

    setTimeout(() => this.filter(), 500);
  }

  getValue() {
    return this.state;
  }
  filter() {
    this.props.onFilter({
      fecha: this.getValue()
    });
  }
  render() {
    return [
      <React.Fragment>
        <div class="input-group input-group-sm mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">
              D
            </span>
          </div>
          <input
            type="date"
            class="form-control"
            aria-label="Sizing example input"
            aria-describedby="inputGroup-sizing-sm"
            name="desde"
            onChange={this.onChange}
          />
        </div>
        <div class="input-group input-group-sm mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">
              H
            </span>
          </div>
          <input
            type="date"
            class="form-control"
            aria-label="Sizing example input"
            aria-describedby="inputGroup-sizing-sm"
            name="hasta"
            onChange={this.onChange}
          />
        </div>
      </React.Fragment>
    ];
  }
}
